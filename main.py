import argparse
from pathlib import Path
from typing import Optional
from mdpydoc.doc_generator import DocGenerator
from mdpydoc.logger import AppLogger

def validate_directory(path: str) -> Path:
    """
    Validate if the given path is a directory and exists.

    Args:
        path (str): The path to validate.

    Returns:
        Path: The validated Path object.

    Raises:
        argparse.ArgumentTypeError: If the path is not a directory or doesn't exist.
    """
    directory = Path(path)
    if not directory.is_dir():
        raise argparse.ArgumentTypeError(f"'{path}' is not a valid directory.")
    if not directory.exists():
        raise argparse.ArgumentTypeError(f"Directory '{path}' does not exist.")
    return directory

def validate_max_workers(value: str) -> Optional[int]:
    """
    Validate the max_workers argument.

    Args:
        value (str): The value to validate.

    Returns:
        Optional[int]: The validated max_workers value.

    Raises:
        argparse.ArgumentTypeError: If the value is not a positive integer.
    """
    if value.lower() == 'none':
        return None
    try:
        ivalue = int(value)
        if ivalue <= 0:
            raise ValueError
        return ivalue
    except ValueError:
        raise argparse.ArgumentTypeError(f"'{value}' is not a valid positive integer or 'None'.")

def main() -> None:
    """
    Main entry point for the documentation generator.

    This function sets up the argument parser, processes command-line arguments,
    initializes the DocGenerator, and starts the documentation generation process.

    Raises:
        argparse.ArgumentTypeError: If there are issues with the provided arguments.
        Exception: For any unexpected errors during execution.

    Example:
        To run the script:
        python script_name.py /path/to/source /path/to/docs --show-code --log-level DEBUG --max-workers 4

    Note:
        Use the --help option to see full usage instructions.

    TODO:
        - Add support for generating documentation in different output formats (e.g., HTML, PDF).
        - Implement a progress bar for large projects.
        - Add an option to exclude specific files or directories from documentation.
    """
    logger = AppLogger(__name__).get_logger()
    logger.info("Starting documentation generator")

    parser = argparse.ArgumentParser(
        description="Generate documentation for Python code.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("src_directory", type=validate_directory, help="Source directory containing Python files")
    parser.add_argument("docs_directory", type=validate_directory, help="Target directory for generated documentation")
    parser.add_argument("--show-code", action="store_true", help="Include source code in documentation")
    parser.add_argument("--log-level", type=str, default="INFO", 
                        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"], 
                        help="Set the logging level")
    parser.add_argument("--max-workers", type=validate_max_workers, default=None, 
                        help="Maximum number of worker threads (positive integer or 'None')")

    try:
        args = parser.parse_args()
        logger.info(f"Parsed arguments: {args}")

        doc_generator = DocGenerator(
            show_code=args.show_code,
            log_level=args.log_level,
            max_workers=args.max_workers
        )
        logger.info("DocGenerator initialized")

        doc_generator.process_directory(args.src_directory, args.docs_directory)
        logger.info("Documentation generation completed successfully")

    except argparse.ArgumentTypeError as e:
        logger.error(f"Argument error: {e}")
        print(f"Argument error: {e}")
        parser.print_help()
    except Exception as e:
        logger.error(f"An unexpected error occurred: {e}", exc_info=True)
        print(f"An unexpected error occurred: {e}")
        raise

if __name__ == "__main__":
    main()